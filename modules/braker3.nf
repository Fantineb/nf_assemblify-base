process braker3 {
        // label is used to find appropriate resources to use with this task.
        // see conf/resources.conf
        label 'highmem'

        // tag is used to display task name in Nextflow logs
        // Here we use input channel file name
        tag "Braker3_for_annotation"

        // Result files published in Netxtflow result directory.
        // See also 'output' directive, below.
        publishDir "${params.resultdir}/05_braker3",  mode: 'copy', pattern: 'braker3/*.gff3'
        publishDir "${params.resultdir}/logs/braker3", mode: 'copy', pattern: 'braker3/*.faa'
        publishDir "${params.resultdir}/logs/braker3",      mode: 'copy', pattern: 'braker3*.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",     mode: 'copy', pattern: 'braker3*.cmd'

        input:
                path(red_ch)
                path(hisat_ch)
                path(proteins_ch)
                val(species_ch)

        // Workflow output stream
        output:
                path("braker3/*.gff3"), emit: braker3_gff3_ch
                path("braker3/*.faa"), emit: braker3_faa_ch
                path("braker3*.cmd")
                path("braker3*.log")

        script:
        """
        braker3.sh $red_ch $hisat_ch $species_ch $proteins_ch ${task.cpus} braker3.cmd >& braker3.log 2>&1
        """
}