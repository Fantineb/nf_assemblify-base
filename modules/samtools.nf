process samtools {
        // label is used to find appropriate resources to use with this task.
        // see conf/resources.conf
        label 'midmem'

        // tag is used to display task name in Nextflow logs
        // Here we use input channel file name
        tag "SAM_to_BAM_indexing"

        // Result files published in Nextflow result directory.
        // See also 'output' directive, below.
        publishDir "${params.resultdir}/04b_samtools",  mode: 'copy', pattern: '*.bam'
        publishDir "${params.resultdir}/04b_samtools",  mode: 'copy', pattern: '*.bai'
        publishDir "${params.resultdir}/logs/samtools", mode: 'copy', pattern: 'samtools*.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",     mode: 'copy', pattern: 'samtools*.cmd'

        input:
		path(hisat_sam_ch)

        // Workflow output stream
        output:
                path("*.bam"), emit: bam_file_ch
                path("*.bai")
                path("samtools*.log")
                path("samtools*.cmd")

        script:
        """
        samtools.sh ${task.cpus} ${hisat_sam_ch} samtools.cmd >& samtools.log 2>&1
        """
}

