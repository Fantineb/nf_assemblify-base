process hisat2 {
        // label is used to find appropriate resources to use with this task.
        // see conf/resources.conf
        label 'highmem'

        // tag is used to display task name in Nextflow logs
        // Here we use input channel file name
        tag "Genome_indexing"

        // Result files published in Nextflow result directory.
        // See also 'output' directive, below.
        publishDir "${params.resultdir}/04a_hisat2",  mode: 'copy', pattern: '*.ht2'
        publishDir "${params.resultdir}/04a_hisat2",  mode: 'copy', pattern: '*.sam'
        publishDir "${params.resultdir}/logs/hisat2", mode: 'copy', pattern: 'hisat2*.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",     mode: 'copy', pattern: 'hisat2*.cmd'

        input:
		path(red_ch)
	 	path(illumina_R1_ch)
		path(illumina_R2_ch)

        // Workflow output stream
        output:
                path("*.ht2")
                path("*.sam"), emit: hisat_sam_ch
                path("hisat2*.log"), emit: hisat_log_ch
                path("hisat2*.cmd")

        script:
        """
        hisat2.sh ${task.cpus} ${red_ch} ${illumina_R1_ch} ${illumina_R2_ch} hisat2.cmd >& hisat2.log 2>&1
        """
}

