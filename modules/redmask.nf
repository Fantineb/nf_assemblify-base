process redmask {
        // label is used to find appropriate resources to use with this task.
        // see conf/resources.conf
        label 'midmem'

        // tag is used to display task name in Nextflow logs
        // Here we use input channel file name
        tag "Repeat_masking_${hifiasm_ch.baseName}"

        // Result files published in Nextflow result directory.
        // See also 'output' directive, below.
        publishDir "${params.resultdir}/03_redmask",  mode: 'copy', pattern: 'masked/*.msk'
        publishDir "${params.resultdir}/logs/redmask", mode: 'copy', pattern: 'redmask*.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",     mode: 'copy', pattern: 'redmask*.cmd'

        input:
		path(hifiasm_ch)

        // Workflow output stream
        output:
                path("masked/*.msk"), emit: redmask_ch
                path("redmask*.log")
                path("redmask*.cmd")

        script:
        """
        redmask.sh ${task.cpus} ${hifiasm_ch} redmask.cmd >& redmask.log 2>&1
        """
}

