process busco {
        // label is used to find appropriate resources to use with this task.
        // see conf/resources.conf
        label 'midmem'

        // tag is used to display task name in Nextflow logs
        // Here we use input channel file name
        tag "QC_${hifiasm_ch.baseName}"

        // Result files published in Netxtflow result directory.
        // See also 'output' directive, below.
        publishDir "${params.resultdir}/02b_busco",  mode: 'copy', pattern: '*.busco'
        publishDir "${params.resultdir}/logs/busco", mode: 'copy', pattern: 'busco*.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",     mode: 'copy', pattern: 'busco*.cmd'

        input:
                path(busco_bank) 
		val(busco_lineage)
		path(hifiasm_ch)

        // Workflow output stream
        output:
                path("*.busco")
                path("busco*.log")
                path("busco*.cmd")

        script:
        """
        busco.sh ${task.cpus} ${busco_bank} ${busco_lineage} ${hifiasm_ch} busco.cmd >& busco.log 2>&1
        """
}


