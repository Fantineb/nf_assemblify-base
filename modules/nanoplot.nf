process nanoplot {
        // label is used to find appropriate resources to use with this task.
        // see conf/resources.conf
        label 'lowmem'

        // tag is used to display task name in Nextflow logs
        // Here we use input channel file name
        tag "QC_${fq_ch.baseName}"

        // Result files published in Netxtflow result directory.
        // See also 'output' directive, below.
        publishDir "${params.resultdir}/01c_nanoplot",  mode: 'copy', pattern: '{*.png,*.html,*.txt}'
        publishDir "${params.resultdir}/logs/nanoplot", mode: 'copy', pattern: 'nanoplot.log'
        publishDir "${params.outdir}/00_pipeline_info/cmd",     mode: 'copy', pattern: 'nanoplot.cmd'

        input:
                each(fq_ch)

        // Workflow output stream
        output:
                path("*.*")
                path("nanoplot.log")
                path("nanoplot.cmd")

        script:
        """
        nanoplot.sh $fq_ch ${task.cpus} nanoplot.cmd >& nanoplot.log 2>&1
        """
}


