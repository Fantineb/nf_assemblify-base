#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##      Quality control of assembly (Hifiasm out files) using BUSCO          ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
CPUS=${args[0]}
DB_PATH=${args[1]}
DB_NAME=${args[2]}
ASSEMBLY_FILE=${args[3]}
LOGCMD=${args[4]}

# Command to execute
RES_FILE=$(basename ${ASSEMBLY_FILE%.*}).busco
CMD="busco -c ${CPUS} --offline -m genome -i ${ASSEMBLY_FILE} -o ${RES_FILE} -l ${DB_PATH}/${DB_NAME}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

