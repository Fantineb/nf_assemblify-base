#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##        Repeat masking of assembly files from Hifiasm using Red            ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
CPUS=${args[0]}
ASSEMBLY_FILE=${args[1]} # Input data = Assembly sequences from Hifiasm
LOGCMD=${args[2]}

# Command to execute
CMD="Red -gnm . -msk masked -cor ${CPUS}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Create a directory for RED output
mkdir -p masked

# Execute command
eval ${CMD}

