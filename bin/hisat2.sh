#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Genome indexing with Hisat2 and SAM to BAM file conversion with Samtools  ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
CPUS=${args[0]}
FASTA_RED_FILE=${args[1]} # Input data = Softmasked asssembly sequences from Red
R1=${args[2]} # Input data = Illumina reads R1
R2=${args[3]} # Input data = Illumina reads R2
LOGCMD=${args[4]}

# Definition of the max lenght of INTRON for mapping (constant) 
INTRON=20000

# Commands to execute

## Creation of the output file of genome indexing (SAM format)
SAM_FILE_OUT=$(basename ${R1%.fastq*}).sam

## Step 1: Genome indexing and reads mapping, conversion of SAM to BAM file 
CMD="hisat2-build -p ${CPUS} ${FASTA_RED_FILE} ${FASTA_RED_FILE%.*} ; \
    hisat2 -p ${CPUS} --no-unal -q -x ${FASTA_RED_FILE%.*} -1 ${R1} -2 ${R2} --max-intronlen ${INTRON} > $SAM_FILE_OUT"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}


