#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##        SAM to BAM file conversion and reads indexing with Samtools        ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
CPUS=${args[0]}
SAM_FILE_IN=${args[1]} # Input data = SAM file from hisat2
LOGCMD=${args[2]}

# Commands to execute

## Creation of the output file (BAM format)
BAM_FILE_OUT=$(basename ${SAM_FILE_IN%.*}).bam

## Step 1: Conversion of SAM to BAM file 
CMD1="samtools sort -o ${BAM_FILE_OUT} -@ ${CPUS} -m 2G -O bam -T tmp ${SAM_FILE_IN}"

## Step 2: Indexing of BAM file
CMD2="samtools index ${BAM_FILE_OUT}"

# Keep command in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}

# Execute command
eval ${CMD1}
eval ${CMD2}

