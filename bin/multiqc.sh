#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##   Results aggregation with multiQC (used for fastqc and hisat2 results)   ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
DIR=${args[0]}
REPORT_NAME=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="multiqc --outdir . --filename $REPORT_NAME ${DIR}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

