#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##           	Quality control of PacBio Hifi files using nanoplot          ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
DIR=${args[0]}
CPUS=${args[1]}
LOGCMD=${args[2]}

# Command to execute
CMD="NanoPlot -t ${CPUS} --plots kde dot hex --N50 \
         --title \"PacBio Hifi reads for $(basename ${DIR%.hifi})\" \
         --fastq ${DIR} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

