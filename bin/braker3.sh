#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                Genome structural annotation with Braker3                  ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
MSK_RED_FILE=${args[0]} # Input data = Output from RED (softmasked assembly sequences)
HISAT_BAM=${args[1]} # Input data = Mapped reads from Hisa2t/Samtools (BAM format)
SPECIES=${args[2]} # Name of the species
PROTEINS=${args[3]} # Proteins from short evolutionary distance
CPUS=${args[4]}
LOGCMD=${args[5]}

# Command to execute
CMD="braker.pl --species=${SPECIES} --genome=${MSK_RED_FILE} --bam=${HISAT_BAM} --prot_seq=${PROTEINS} \
          --workingdir=braker3 --gff3 --threads ${CPUS}"

# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD