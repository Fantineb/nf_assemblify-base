Pull requests, bug reports, and all other forms of contribution are welcomed and highly encouraged!

Pull requests:
Please do not hesitate to contribute to the project.  We will take a look at your request ! 

Asking Questions
If you have any question, you can directly ask us via our mail or in the Feedback part of the readme

Bug reports:
If you have encountered some issues concerning our code, please contact us via email or directly in the issue track


