// Load processes parameters
includeConfig 'conf/base.config'
// Load resources configuration
includeConfig 'conf/resources.config'

manifest {
    name = 'Assemblify'
    author = 'Fantine BENOIT & Ambre BAILLOU'
    description = ''
    homePage = ''
    mainScript = 'main.nf'
    nextflowVersion = '>=23.04.0'
    version = ''
}

// Define env variable that will be accessible in nextflow tasks
env {
    WDir = "${SCRATCH}"
}


// Execution profiles
profiles {
    custom {
        includeConfig 'conf/custom.config'
        includeConfig 'conf/reports.config'
        // Workdir for temporary data
        workDir = "$env.WDir/assemblify_workdir/$params.projectName"
    }
    singularity {
        docker.enabled = false
        singularity.autoMounts = true
        singularity.enabled = true
        process {
            withName : fastqc {
                container = "quay.io/biocontainers/fastqc:0.11.9--hdfd78af_1"
            }
            withName : multiqc {
                container = "quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0"
            }
            withName : hifiasm {
                container = "quay.io/biocontainers/hifiasm:0.18.9--h5b5514e_0"
            }
            withName : nanoplot {
                container = "quay.io/biocontainers/nanoplot:1.32.1--py_0"
            }
            withName : busco {
                container = "quay.io/biocontainers/busco:5.5.0--pyhdfd78af_0"
            }
	    withName : redmask {
                container = "quay.io/biocontainers/red:2018.09.10--h9f5acd7_0"
            }
            withName : hisat2 {
                container = "quay.io/biocontainers/hisat2:2.2.1--h87f3376_5"
            }
	    withName : samtools {
                container = "quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0"
            }
            withName : multiqc_hisat2 {
                container = "quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0"
            }
            withName : braker3 {
                container = "teambraker/braker3:v3.0.7.1"
            }
        }
    }
}

// Function to ensure that resource requirements don't go beyond
// a maximum limit
def check_max(obj, type) {
  if (type == 'memory') {
    try {
      if (obj.compareTo(params.max_memory as nextflow.util.MemoryUnit) == 1)
        return params.max_memory as nextflow.util.MemoryUnit
      else
        return obj
    } catch (all) {
      println "   ### ERROR ###   Max memory '${params.max_memory}' is not valid! Using default value: $obj"
      return obj
    }
  } else if (type == 'time') {
    try {
      if (obj.compareTo(params.max_time as nextflow.util.Duration) == 1)
        return params.max_time as nextflow.util.Duration
      else
        return obj
    } catch (all) {
      println "   ### ERROR ###   Max time '${params.max_time}' is not valid! Using default value: $obj"
      return obj
    }
  } else if (type == 'cpus') {
    try {
      return Math.min( obj, params.max_cpus as int )
    } catch (all) {
      println "   ### ERROR ###   Max cpus '${params.max_cpus}' is not valid! Using default value: $obj"
      return obj
    }
  }
}
