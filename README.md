[![License: GNU AGPLv3](https://img.shields.io/badge/License-GNU%20AGPLv3-rgba)](https://www.gnu.org/licenses/agpl-3.0.html)

[![Contributors](https://img.shields.io/badge/Contributors-2-red)](#contributors)

#  Assemblify-base pipeline: FAIR principles applied to genome assembly and annotation

Date: 2024-01-31.

This is a project associated to this [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).




## Introduction
### Genome Sequencing and Analysis of *Dunaliella primolecta*

The genome of *Dunaliella primolecta* has been sequenced and is now available under the GenBank accession number `GCA_914767535.2`. 

The objective of this project is to perform genome assembly and annotation, providing a comprehensive insight of *Dunaliella primolecta*, using diverse bioinformatics tools. 

For this project, we have two types of raw data: 

- Illumina reads: RNA-sequencing (RNAseq)
- Pacbio: HiFi


### Project Overview:

- **Organism:** *Dunaliella primolecta*
- **Genomic Accession:** `GCA_914767535.2`
- **WGS Project:** CAJZBB02
- **Assembly Type:** Haploid (Principal haplotype of diploid)
- **Submitter:** Wellcome Sanger Institute
- **Submission Date:** July 14, 2023

## Prerequisites

Before getting started, make sure the following tools are installed on your system:

- [Slurm](https://slurm.schedmd.com/overview.html) 
- [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html)
- [Singularity](https://sylabs.io/guides/3.9/user-guide/quick_start.html)

Under Slurm cluster workload manager, shell scripts automatically execute by Nextflow via a Singularity image.


For this project we used the IFB cluster **IFB-core** via [Open-on-Demand](https://ondemand.cluster.france-bioinformatique.fr/pun/sys/dashboard). IFB-core cluster is composed by 4300 cores and 2Pb of storage. Its aim is to provide access to many  bioinformatics resources.

Access to the cluster is via an ssh key. Once connected, the user can request the infrastructure via the SLURM submission system. Hereafter called: *"run_assemblify.slurm"*. 

For more information check [IFB-core](https://france-bioinformatique.fr/cluster-ifb-core/).

The computer used to run the analysis:

- Windows 10
- 64 cpus
- 250 Go ram

## How to Use

Clone this repository to your local machine:

   ```bash
   git clone [repo-link]
  ```

If you clone it in your home repository, it is available as: `./nf_assemblify-base`.

## Pipeline 

### Overview

Step-by-step pipeline for genome assembly and annotation: 
![](https://gitlab.com/Fantineb/nf_assemblify-base/-/raw/master/Pipeline_overview.png)


Some steps have not been fully completed here (BRAKER3 and BUSCO post BRAKER3). For **BRAKER3**, we created the files to execute in order to run the step (braker3.nf, braker3.sh) and adapted the main.nf, nextflow.config and custom.config files, but it is not possible to execute it because of unsolved errors.

### Detailed steps

Here are all the steps implemented with each tool used:

**Step 1. Data quality control:**

For Pacbio data, quality is evaluated with [Nanoplot](https://anaconda.org/bioconda/nanoplot) (version 1.32.1, and related used Biocontainer [nanoplot:1.32.1--py_0](https://quay.io/biocontainers/nanoplot:1.32.1--py_0)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/nanoplot.sh
```


For RNAseq data (Illumina reads), quality is evaluated with [FastQC](https://anaconda.org/bioconda/fastqc) (version 0.11.9, and related used Biocontainer [fastqc:0.11.9--hdfd78af_1](https://quay.io/biocontainers/fastqc:0.11.9--hdfd78af_1)) and results are aggregated with [MultiQC](https://anaconda.org/bioconda/Multiqc) (version 1.14, and related used Biocontainer [multiqc:1.14--pyhdfd78af_0](https://quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0)). Corresponding scripts are:

```bash
   ./nf_assemblify-base/bin/fastqc.sh
   ./nf_assemblify-base/bin/multiqc.sh
```


**Step 2. Assembly:**

First, genome assembly from PacBio reads is realized with [HIFIasm](https://anaconda.org/bioconda/HIFIasm) (version 0.18.9, and related used Biocontainer [hifiasm:0.18.9--h5b5514e_0](https://quay.io/biocontainers/hifiasm:0.18.9--h5b5514e_0)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/hifiasm.sh
```

 Then, genome assembly completness is evaluated with [BUSCO](https://anaconda.org/bioconda/BUSCO) (version 5.5.0, and related used Biocontainer [busco:5.5.0--pyhdfd78af_0](https://quay.io/biocontainers/busco:5.5.0--pyhdfd78af_0)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/busco.sh
```


**Step 3. Repeat masking:** 

Genome assembly repeats softmasking (identication of repetitions and hiding them for structural annotation) is realized with [REDmask](https://anaconda.org/bioconda/red) (version 2018.09.10, and related used Biocontainer [red:2018.09.10--h4ac6f70_2](https://quay.io/biocontainers/red:2018.09.10--h4ac6f70_2)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/redmask.sh
```

**Step 4. Mapping:**
 
First step of the mapping is genome indexing, realized with [Hisat2](https://anaconda.org/bioconda/Hisat2) (version 2.2.1, and related used Biocontainer [hisat2:2.2.1--h87f3376_5](https://quay.io/biocontainers/hisat2:2.2.1--h87f3376_5)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/hisat2.sh
```
Second step, SAM to BAM file conversion and reads indexing, is realized with [Samtools](https://anaconda.org/bioconda/Samtools) (version 1.19.2, and related used Biocontainer [samtools:1.19.2--h50ea8bc_0](https://quay.io/biocontainers/samtools:1.19.2--h50ea8bc_0)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/samtools.sh
```

Results are aggregated with [MultiQC](https://anaconda.org/bioconda/Multiqc) (version 1.14, and related used Biocontainer [multiqc:1.14--pyhdfd78af_0](https://quay.io/biocontainers/multiqc:1.14--pyhdfd78af_0)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/multiqc.sh
```


**Step 5. Prediction and annotation:**

Genome structural annotation is realized with
 [braker3](https://anaconda.org/bioconda/braker3) (version 3.0.3, and related used container [braker3:v3.0.7.1](https://teambraker/braker3:v3.0.7.1)). Corresponding script is:

```bash
   ./nf_assemblify-base/bin/braker3.sh
```

## Input/output data

The input and output data for each tool used in the workflow is detailed here.

STEP | **TOOL** | INPUT DATA | OUTPUT DATA OF INTEREST
-----|----------|------------|------------------------
Data quality control| **fastQC** | Illumina sequencing files (.fastq.gz) | Web report (.html)
. | (more) |  | Archive (.zip)
. | **multiQC** | Archive from **fastQC** (.zip) | Web report (.html)
. | (more) | **OR** Log files from **Hisat2** (.log) |  
. | **Nanoplot** | PacBio sequencing files (.fasta.gz) | Web report (.html)
. | (more) |  | Plot (.png)
. | (more) |  | Statistics (.txt)
Assembly | **Hifiasm** | PacBio sequencing files (.fasta.gz) | Assembly sequences (.fasta)
. |(more) |  | Assembly graph (.gfa)
. | **BUSCO** | Assembly sequences from **Hifiasm** (.fasta) | Statistics (.tsv)
Repeat masking | **REDmask** | Assembly sequences from **Hifiasm** (.fasta) | Softmasked assembly sequences (.msk)
Mapping | **hisat2** | Illumina sequencing files (.fastq.gz) | Genome index (.ht2)
. | (more) | Softmasked assembly sequences from **REDmask** (.msk) | Mapping file (.bam)
. |(more) |  | Mapping file index (.bai)
Prediction and annotation | **braker3** | Softmasked assembly sequences from **REDmask** (.msk) | Structural annotation (.gff3)  
. |(more) | Mapped reads from **hisat2** (.bam) | Proteins sequences from annotation (.faa)
. |(more) |  Proteins from short evolutionary distance (.faa) | 
. | **BUSCO** | Annotated sequences from **braker3** (.faa) | Statistics (.tsv)

## Architecture of the project

- **conf directory:**
  Contains the configuration files for the workflow.
- **conf/resources.config file:**
  Contains RAM/CPU/TIME specifications used by tools in the workflow.
- **bin directory:**
  Contains the shell scripts that execute tools.
- **modules directory:**
  Contains Nexflow steps (processes) for the overall pipeline. Each module handles input/output channels and resource specifications, calls shell scripts located in 'bin,' and publishes results.
- **main.nf file:**
  Describes the chaining of modules, essentially representing the workflow itself.
- **nextflow.config file:**
  Sets up the Singularity images to be used during pipeline execution.
- **ifb-core.config file:**
  Contains resource definitions for executing the pipeline on the IFB Core Cluster.
- **run_assemblify.slurm file:**
  A helper script to submit pipeline execution on a cluster.
- **clean.sh file:**
  A script to clean the working directory and discard temporary files related to executed processes. To use carefully according to your needs.
- **.gitignore file:**
  Contains list of files to be ignored for commit (eg temporary files, results repository).
- **scripts directory:**
  Contains the scripts used to run a sample genome assembly. A specific README.md is associated with.

Here is the **file hierarchy** of the git project, and the associated command line to get it from your home repository:

```bash
$ cd nf_assemblify-base/
$ tree -L 3
.
|-- CONTRIBUTING.md
|-- LICENSE
|-- README.md
|-- bin
|   |-- braker3.sh
|   |-- busco.sh
|   |-- fastqc.sh
|   |-- hifiasm.sh
|   |-- hisat2.sh
|   |-- multiqc.sh
|   |-- nanoplot.sh
|   |-- redmask.sh
|   `-- samtools.sh
|-- clean.sh
|-- conf
|   |-- base.config
|   |-- custom.config
|   |-- reports.config
|   `-- resources.config
|-- ifb-core.config
|-- main.nf
|-- modules
|   |-- braker3.nf
|   |-- busco.nf
|   |-- fastqc.nf
|   |-- hifiasm.nf
|   |-- hisat2.nf
|   |-- multiqc.nf
|   |-- multiqc_hisat2.nf
|   |-- nanoplot.nf
|   |-- redmask.nf
|   `-- samtools.nf
|-- nextflow.config
|-- results
|   `-- assemblify
|       |-- 00_pipeline_info
|       |-- 01_reports
|       `-- 02_results
|-- run_assemblify.slurm
`-- scripts
    |-- 01a-fastqc.pbs
    |-- 01b-multiqc.pbs
    |-- 01c-nanoplot.pbs
    |-- 02a-hifiasm.pbs
    |-- 02b-busco.pbs
    |-- 03-redmask.pbs
    |-- 04-hisat2.pbs
    `-- README.md
```

And this is the file hierarchy of the `./nf_assemblify-base/results/assemblify` results repository:

```bash
.
|-- 00_pipeline_info
|   |-- base.config
|   `-- cmd
|-- 01_reports
|   |-- dag.svg
|   |-- report.html
|   |-- timeline.html
|   `-- trace.tsv
`-- 02_results
    |-- 01a_fastqc
    |-- 01b_multiqc
    |-- 01c_nanoplot
    |-- 02a_hifiasm
    |-- 02b_busco
    |-- 03_redmask
    |-- 04a_hisat2
    |-- 04b_samtools
    |-- 04c_multiqc_hisat2
    `-- logs
```

## How to run the workflow
You can follow the following steps to run the scripts. You will need to adapt the scripts depending on your working directory.


* In your home repository, create a working directory `./assemblify-tmp` as follows:

 ```bash
   mkdir assemblify-tmp
   ```

*NB*: Our working directory is `/shared/projects/2401_m2_bim/app-2401-04/assemblify-tmp`.


* The working directory have to be modified in the `./nf_assemblify-base/run_assemblify.slurm` script:

 ```bash
   WK_DIR=/shared/projects/2401_m2_bim/app-2401-04/assemblify-tmp
   ```
   
* To run the pipeline, execute the following command line in a bash terminal:

 ```bash
   sbatch run_assemblify.slurm 
   ```

* The progress of the process is available in the following log file:

 ```bash
   ./nf_assemblify-base/assemblify.log 
   ```

## Authors

- [Ambre Baillou](https://gitlab.com/abaillou) 
- [Fantine Benoit](https://gitlab.com/Fantineb)

Both Master students in bioinformatics at Rennes 1 University (Master mention Bio-informatique parcours Informatique pour la biologie et la santé).

## Acknowledgements 

We would like to thank each following scientist for their teachings and support as part of our Master's module "FAIR principles applicated to genome assembly and annotation" (January 2024):
- [Anthony Bretaudeau](https://orcid.org/0000-0003-0914-2470)
- [Alexandre Cormier](https://orcid.org/0000-0002-7775-8413)
- [Erwan Corre](https://orcid.org/0000-0001-6354-2278)
- [Patrick Durand](https://orcid.org/0000-0002-5597-4457)
- [Gildas  Le Corguillé](https://orcid.org/0000-0003-1742-9711)
- [Stéphanie Robin](https://orcid.org/0000-0001-7379-9173)

## Sources

 - [Original project](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/)
 - [Bactopia](https://github.com/bactopia/bactopia/tree/master)
 - [Wf-pore-c](https://github.com/epi2me-labs/wf-pore-c)
 - [Ki-Next](https://gitlab.ifremer.fr/bioinfo/workflows/kinnext)


## Feedback

Your feedback can be very useful for us and the community. If you have encountered any issues using our **nf_assemblify_project**, or if you have some questions and even new ideas to improve this wonderful project, do not hesitate to contact us via email `fantine.benoit@etudiant.univ-rennes1.fr` or `ambre.baillou@etudiant.univ-rennes1.fr`.
 
## License

This project is under GNU AGPLv3 license.




